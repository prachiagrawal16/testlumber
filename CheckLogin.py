from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from pyvirtualdisplay import Display
import unittest, time


class TestCase2(unittest.TestCase):
    def setUp(self):
        display = Display(visible=0, size=(800, 800))
        display.start()
        self.driver = webdriver.Chrome(executable_path='/home/prachi/PycharmProjects/TestSelenium/chromedriver')
        #self.driver = webdriver.Chrome(executable_path='/home/prachi/PycharmProjects/TestSelenium/chromedriver')
        self.driver.implicitly_wait(30)
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_case2(self):
        driver = self.driver
        driver.get("https://rdt84ui-test.azurewebsites.net/")
        driver.find_element_by_id("input_0").click()
        driver.find_element_by_id("input_0").clear()
        driver.find_element_by_id("input_0").send_keys("barberi.alex@gmail.com")
        driver.find_element_by_xpath("//md-input-container[2]").click()
        driver.find_element_by_id("input_1").click()
        driver.find_element_by_id("input_1").clear()
        driver.find_element_by_id("input_1").send_keys("Exp3d!t3")
        driver.find_element_by_id("input_1").send_keys(Keys.ENTER)
        time.sleep(10)
        driver.find_element_by_xpath("//div[@class ='buttons-header']/div[3]//button[2]").click()
        time.sleep(5)
        driver.find_element_by_xpath("//div[3]/button[3]").click()
        time.sleep(5)
        driver.find_element_by_xpath("//button[4]").click()
        time.sleep(5)
        driver.find_element_by_xpath("//div[3]/button").click()
        time.sleep(5)
        driver.find_element_by_xpath("//rdt-calendar-day-view/div/button/span").click()
        time.sleep(5)
        driver.find_element_by_xpath("//div[3]/button[3]").click()
        time.sleep(5)
        driver.find_element_by_xpath("//rdt-calendar-month-view/div/button/span").click()
        time.sleep(5)
        driver.find_element_by_xpath("(//input[@type='checkbox'])[15]").click()
        time.sleep(5)
        driver.find_element_by_id("select_6").click()
        time.sleep(5)
        driver.find_element_by_id("select_option_16").click()
        time.sleep(5)
        driver.find_element_by_id("select_9").click()
        time.sleep(2)
        driver.find_element_by_id("select_option_23").click()
        time.sleep(5)
        driver.find_element_by_id("select_12").click()
        time.sleep(5)
        driver.find_element_by_id("select_option_27").click()
        time.sleep(5)
        driver.find_element_by_xpath("//span").click()
        time.sleep(5)
        driver.find_element_by_xpath("//img").click()
        time.sleep(5)
        driver.find_element_by_xpath("//rdt-calendar-month-view/div/button/span").click()
        time.sleep(5)
        driver.find_element_by_xpath("//a/div").click()
        time.sleep(5)
        driver.find_element_by_link_text("Logout").click()
        time.sleep(5)
        driver.close()

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest.main()
